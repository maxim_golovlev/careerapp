//
//  Answer.swift
//  CareerApp
//
//  Created by Admin on 22.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Answer {
    struct Sides {
        let good: String
        let bad: String
    }
    
    let sides: Sides
    let description: String
    let jobs: String
    let name: String
    let letters: String
    let secret: String
    let type: String
    
    static func initWith(dict: [String: AnyObject]) -> Answer? {
        
        guard let letters = dict["letters"] as? String,
            let name = dict["name"] as? String,
            let description = dict["discription"] as? String,
            let jobs = dict["jobs"] as? String,
            let good = dict["strong_sides"] as? String,
            let bad = dict["weak_sides"] as? String,
            let secret = dict["secret"] as? String,
            let type = dict["type"] as? String
            else { return nil }
        
        let sides = Sides(good: good, bad: bad)
        
        let answer = Answer(sides: sides, description: description, jobs: jobs, name: name, letters: letters, secret: secret, type: type)
        
        return answer
    }
}
