//
//  SavedAnswer.swift
//  CareerApp
//
//  Created by Admin on 16.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

struct SavedAnswer: Encodable, Decodable {
    let id: String
    let text: String
    let date: Date
}
