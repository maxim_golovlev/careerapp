//
//  Question.swift
//  CareerApp
//
//  Created by Admin on 22.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct Question {
    
    struct Option {
        let text: String
        let letter: String
    }
    
    struct Subquestion {
        var first: Option?
        var second: Option?
    }
    
    var subquestions: [Subquestion]
    var letter = ""
    
    static func initWith(dict: [String: AnyObject]) -> Question? {
        
        guard let first = dict["first"] as? [String: AnyObject],
            let second = dict["second"] as? [String: AnyObject],
            let firstLetter = first["letter"] as? String,
            let secondLetter = second["letter"] as? String,
            let firstQuestions = first["description"] as? [String],
            let secondQuestions = second["description"] as? [String]
            else { return nil }
        
        let firstOptions = firstQuestions.flatMap({ Option.init(text: $0, letter: firstLetter) })
        var secondOptions = secondQuestions.flatMap({ Option.init(text: $0, letter: secondLetter) })
        
        var subquestions = firstOptions.flatMap({ Subquestion.init(first: $0, second: nil) })
        
        _ = subquestions.enumerated().flatMap({ subquestions[$0.offset].second = secondOptions[$0.offset] })
        
        let question = Question.init(subquestions: Array(subquestions.shuffled().prefix(5)), letter: "")
        
        return question
    }
}
