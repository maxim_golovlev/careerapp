//
//  AnswerCell.swift
//  CareerApp
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

class AnswerCell: UITableViewCell, ConfigurableCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return 54
    }
    
    func configure(with data:(title: String?, subtitle: String?)) {
        titleLabel.text = data.title
        dateLabel.text = data.subtitle
    }
}
