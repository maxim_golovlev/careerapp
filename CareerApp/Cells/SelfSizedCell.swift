//
//  SelfSizedCell.swift
//  ElvisCorner
//
//  Created by Admin on 13.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class SelfSizedCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    static var defaultHeight: CGFloat? {
        return UITableView.automaticDimension
    }
    
    static var estimatedHeight: CGFloat? {
        return 20
    }
    
    func configure(with title: NSAttributedString?) {
        titleLabel.attributedText = title
        selectionStyle = .none
    }
}
