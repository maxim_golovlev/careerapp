//
//  TextCell.swift
//  CareerApp
//
//  Created by Admin on 04.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class TextCell: UICollectionViewCell {
    
    @IBOutlet weak var textLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 79, right: 0)
        }
    }
    
    func configure(text: String) {
        textLabel.text = text
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }

}
