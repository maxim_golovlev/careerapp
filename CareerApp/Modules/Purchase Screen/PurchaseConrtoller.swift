//
//  PurchaseConrtoller.swift
//  CareerApp
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Firebase
import StoreKit
import Reachability

class PurchaseConrtoller: UIViewController, BaseView {
    
    @IBOutlet weak var purchaseBtn: UIButton!
        {
        didSet {
            purchaseBtn.titleLabel?.minimumScaleFactor = 0.5
            purchaseBtn.titleLabel?.numberOfLines = 1
            purchaseBtn.titleLabel?.adjustsFontSizeToFitWidth = true
            
            setupPurchaseButtonTitle()
        }
    }
    
    var testManager = TestManager.shared
    var reachability: Reachability! {
        return (UIApplication.shared.delegate as? AppDelegate)?.reachability
    }
    
    var noConnection = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        title = "Узнать ответ"
        
        NotificationCenter.default.addObserver(self, selector: #selector(transactionStatusChanged), name: AppConstants.NotificationKeys.transaction, object: nil)
    }
    
    @objc func transactionStatusChanged(_ notification: Notification) {
        
        guard let status = notification.object as? SKPaymentTransactionState else { return }
        
        switch status {
        case .purchasing :
            startLoading()
            break
        case .purchased, .restored  :
            stopLoading()
            if let answer = testManager.answer {
                LocalStorage.appendAnswer(title: answer.type, date: Date())
                openAnswerScreen()
            }
        default:
            stopLoading()
            break
        }
    }
    
    @IBAction func purchaseTapped(_ sender: Any) {
        if reachability.connection == .none
        {
            showAlert(title: "Ошибка", message: "Проверьте соединение с интернетом")
            return
        }
        
        IAPService.shared.purchase(product: .consumable)
    }
    
    @IBAction func declineTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func setupNavBar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ,
                                                                        NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 18)]
    }
    
    func openAnswerScreen() {
        if let nav = Storyboard.Answer.initialViewController() as? UINavigationController {
            if let vc = nav.viewControllers.first as? AnswerController {
                vc.answer = testManager.answer
                self.present(nav, animated: true, completion: nil)
            }
        }
    }

func setupPurchaseButtonTitle() {
    
    if let product = IAPService.shared.products.first(where: {$0.productIdentifier == IAPProducts.consumable.rawValue}) {
        let price = product.price as Decimal
        if price < 299 {
            let attrTitle = NSMutableAttributedString(string: "Узнать ответ за 299 \(price) руб.").addStrikethrough(slice: "299").set(font: FontFamily.Roboto.black.font(size: 20)).add(color: .white)
            purchaseBtn.setAttributedTitle(attrTitle, for: .normal)
        } else {
            purchaseBtn.setTitle("Узнать ответ за \(price) руб.", for: .normal)
        }
    }
}
}


