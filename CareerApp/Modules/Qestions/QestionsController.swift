//
//  QestionsController.swift
//  CareerApp
//
//  Created by Admin on 21.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import BubbleTransition

class QestionsController: UIViewController, BaseView {
    
    @IBOutlet weak var carouselView: iCarousel! {
        didSet {
            carouselView.type = .coverFlow
            carouselView.isPagingEnabled = true
            carouselView.isScrollEnabled = false
        }
    }
    @IBOutlet weak var pageController: QuestionControl!
    
    var questions = [Question.Subquestion]()
    var currentIndex = Int(0)
    var testManager = TestManager.shared
    var prevPurchseType: IAPProducts = .consumable
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("app_name", comment: "")
        setupNavNar()
        
        if let questions = DataManager.shared.fetchQuestions() {
        
            self.questions = questions.reduce(into: [Question.Subquestion](), { (result, question) in
               result = result + question.subquestions
            })
            
            pageController.numberOfItems = self.questions.count
            
            carouselView.reloadData()
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        testManager.refresh()
    }
    
    func setupNavNar() {
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.black.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 2.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 0.3
        self.navigationController?.navigationBar.layer.masksToBounds = false
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                                        NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 18)]
    }
}

extension QestionsController: iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return questions.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        var newView: QestionView
        
        let view: QestionView? = UIView.fromNib()
        newView = view!
        newView.delegate = self
        newView.frame = CGRect(origin: .zero, size: carouselView.bounds.size)

        let question = questions[index]
        newView.update(question: question)
        
        return newView
    }
}

extension QestionsController: iCarouselDelegate {
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        
        switch option {
        case .spacing:
            return 1
        case .wrap:
            return 0
        case .tilt:
            return 0.5
        case .showBackfaces:
            return 1
        default:
            return value
        }
    }
    
    func carouselWillBeginScrollingAnimation(_ carousel: iCarousel) {
        self.view.isUserInteractionEnabled = false
    }
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
        self.view.isUserInteractionEnabled = true
        currentIndex = Int(carousel.scrollOffset)
        pageController.currentPage = Int(carousel.scrollOffset)
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if index < carousel.numberOfItems {
            carousel.scrollToItem(at: index + 1, animated: true)
        }
    }
}

extension QestionsController: QestionViewDelegate {
    
    func didSelect(view: QestionView, answer: Question.Option) {
        let index = carouselView.index(ofItemView: view)
        print(index)
        
        testManager.maxAnswers = self.questions.count
        testManager.append(letter: answer.letter, index: index)
        
        self.view.isUserInteractionEnabled = false
 
        if index != self.carouselView.numberOfItems - 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(700) , execute: {
                self.carouselView.scrollToItem(at: index + 1, animated: true)
            })
        } else {

            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(700) , execute: {
                self.view.isUserInteractionEnabled = true

//                if let savedAnswerKeys = LocalStorage.getAnswerKeys,
//                    let answer = self.testManager.answer,
//                    savedAnswerKeys.contains(where: { $0.key == answer.type }) {
//
//                    self.openAnswerScreen()
//
//                } else {
                
                    let vc = Storyboard.Purchase.initialViewController()
                    self.present(vc, animated: true, completion: nil)
                    
              //  }
            })
        }
    }
    
    func openAnswerScreen() {
        if let nav = Storyboard.Answer.initialViewController() as? UINavigationController {
            if let vc = nav.viewControllers.first as? AnswerController {
                vc.answer = testManager.answer
                self.present(nav, animated: true, completion: nil)
            }
        }
    }
}

