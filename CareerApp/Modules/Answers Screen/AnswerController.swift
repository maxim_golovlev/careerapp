//
//  AnswerControllerViewController.swift
//  CareerApp
//
//  Created by Admin on 04.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import SwiftRater

class StateLabel: UILabel {
    var isSelected: Bool = false {
        didSet {
            textColor = isSelected ? .white : UIColor.init(white: 1, alpha: 0.7)
        }
    }
}

class AnswerController: UIViewController, BaseView {
    
    private let cellId = "cellId"
    private var sides = [String]()
    private var oldScrollOffset: CGPoint = .zero
    private var typeName = "Unknown Type"
    var answer: Answer?
    var testManager = TestManager.shared
    private var textLabel: UILabel = {
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .white
        label.text = ""
        return label
    }()
    
    @IBOutlet weak var leftLabel: StateLabel!
    @IBOutlet weak var rightLabel: StateLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var sliderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var slider: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView! {
        didSet {
            collectionView.register(UINib.init(nibName: "TextCell", bundle: nil), forCellWithReuseIdentifier: cellId)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavNar()
        leftLabel.isSelected = true
        rightLabel.isSelected = false
        
        imageView.applyMotionEffect(magnitude: 30)
        
        if let answer = answer {
            self.sides = self.sides + [answer.sides.good, answer.sides.bad]
            self.typeName = answer.letters
            imageView.image = UIImage.init(named: answer.letters.lowercased())
            title = answer.type
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func setupNavNar() {
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:  #colorLiteral(red: 0, green: 0.5450980392, blue: 0.537254902, alpha: 1),
                                                                        NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 18)]
    }
    
    @IBAction func showDetailTapped(_ sender: UIButton) {
        
        if let nav = Storyboard.AnswerDetail.initialViewController() as? UINavigationController {
            if let vc = nav.viewControllers.first as? AnswerDetail {
                vc.answer = self.answer
                nav.modalTransitionStyle = .flipHorizontal
                present(nav, animated: true, completion: nil)
            }
        }
    }
    
    func toggleButtons() {
        leftLabel.isSelected = !leftLabel.isSelected
        rightLabel.isSelected = !rightLabel.isSelected
        rightLabel.isSelected = !leftLabel.isSelected
        sliderLeadingConstraint.constant = leftLabel.isSelected ? 0 : AppScreen.width / 2
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        let index = leftLabel.isSelected ? 0 : 1
        collectionView.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .centeredHorizontally, animated: true)
        
         oldScrollOffset.x = oldScrollOffset.x == 0 ? AppScreen.width : 0
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        SwiftRater.rateApp(host: self)
        testManager.refresh()
       
        if presentingViewController?.presentingViewController == nil {
            dismiss(animated: true, completion: nil)
        }
        
        if presentingViewController?.presentingViewController?.presentingViewController == nil {
            presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        
        presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        showShareActivity(answer: answer)
    }
    
    @IBAction func sidesTapped(_ gesture: UITapGestureRecognizer) {
        
        if let label = gesture.view as? StateLabel {
            if label.isSelected { return }
            toggleButtons()
        }
    }
}

extension AnswerController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
        
        if let cell = cell as? TextCell, sides.count >= 2 {
                cell.configure(text: sides[indexPath.item])
        }
        
        return cell
    }
}

extension AnswerController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if oldScrollOffset.y < scrollView.contentOffset.y ||
            oldScrollOffset.y > scrollView.contentOffset.y {
            return
        }
        
        if scrollView.contentOffset.x != oldScrollOffset.x {
            toggleButtons()
        }
    }
}

extension AnswerController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: AppScreen.width, height: collectionView.frame.height)
    }
}
