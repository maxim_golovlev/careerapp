//
//  AnswerDetail.swift
//  CareerApp
//
//  Created by Admin on 07.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

class AnswerDetail: UIViewController, BaseView {

    var answer: Answer?
    var testManager = TestManager.shared
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 79, right: 0)
        }
    }
    
    var tableDirector: TableDirector!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavNar()

        title = answer?.type
        
        tableDirector.clear()
        
        let section = TableSection()
        
        if let title = answer?.name, let desription = answer?.description {
            let text = NSMutableAttributedString(string: title + "\n\n" + desription).set(font: FontFamily.Roboto.regular.font(size: 16), text: nil).set(font: FontFamily.Roboto.medium.font(size: 16), text: title)
            let row = TableRow<SelfSizedCell>.init(item: text)
            section.append(row: row)    
        }
        
        if let jobs = answer?.jobs {
            let text = NSMutableAttributedString(string: "   Консультант по профессиональной ориентации\n\n" + jobs).set(font: FontFamily.Roboto.regular.font(size: 16), text: nil).set(font: FontFamily.Roboto.medium.font(size: 16), text: "Консультант по профессиональной ориентации")
            let row = TableRow<SelfSizedCell>.init(item: text)
            section.append(row: row)
        }
        
        if let secret = answer?.secret {
            let text = NSMutableAttributedString(string: "   Секрет успеха\n\n" + secret).set(font: FontFamily.Roboto.regular.font(size: 16), text: nil).set(font: FontFamily.Roboto.medium.font(size: 16), text: "Секрет успеха")
            let row = TableRow<SelfSizedCell>.init(item: text)
            section.append(row: row)
        }
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
    
    @IBAction func shareTapped(_ sender: Any) {
        showShareActivity(answer: answer)
    }
    
    @IBAction func showMainVC(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setupNavNar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ,
                                                                        NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 18)]
    }

}
