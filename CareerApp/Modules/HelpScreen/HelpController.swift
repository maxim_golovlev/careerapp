//
//  HelpController.swift
//  CareerApp
//
//  Created by Admin on 21.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import TableKit

class HelpController: UIViewController {

    @IBOutlet weak private var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
            tableView.tableFooterView = UIView()
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            tableView.contentInset = insets
        }
    }
    
    var tableDirector: TableDirector!
    var topLabelText: NSAttributedString?
    var botLabelText: NSAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavNar()
        
        tableDirector.clear()
        
        let section = TableSection()
        
        let rows = [topLabelText, botLabelText].flatMap({ TableRow<SelfSizedCell>.init(item: $0) })
        
        section.append(rows: rows)
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
        
    }
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func setupNavNar() {
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}
