//
//  WalkthroughController.swift
//  CareerApp
//
//  Created by Admin on 21.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import BWWalkthrough

class WalkthroughController: BWWalkthroughViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        title = "Результаты"

        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let pageOne = stb.instantiateViewController(withIdentifier: "page1")
        
        self.delegate = self
        add(viewController: pageOne)
    }
    
    func setupNavBar() {
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ,
                                                                        NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 18)]
    }
    
    @IBAction func resultsTapped(_ sender: Any) {
        let vc = Storyboard.Results.instantiateViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension WalkthroughController: BWWalkthroughViewControllerDelegate {
    
    func walkthroughCloseButtonPressed() {
        let vc = Storyboard.Qestion.initialViewController() as! UINavigationController
        present(vc, animated: true, completion: nil)
    }
}
