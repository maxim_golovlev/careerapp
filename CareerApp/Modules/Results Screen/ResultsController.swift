//
//  ResultsController.swift
//  CareerApp
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import TableKit

class ResultsController: UIViewController {

    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableDirector = TableDirector(tableView: tableView)
            
            tableView.tableFooterView = UIView()
            tableView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        }
    }
    
    var tableDirector: TableDirector!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "История"
        
        tableDirector.clear()
        
        let section = TableSection()
        
        if let savedKeys = LocalStorage.getAnswers?.sorted(by: { $0.date > $1.date }){
            
            let rows = savedKeys.flatMap({ TableRow<AnswerCell>.init(item: (title: $0.text , subtitle: $0.date.toString("d MMM yyyy, HH:mm"))).on(.click) { [unowned self] options in
                
                guard let cell = options.cell, let answerTitle = cell.titleLabel.text else { return }
                
                    if let nav = Storyboard.Answer.initialViewController() as? UINavigationController {
                        if let vc = nav.viewControllers.first as? AnswerController {
                            vc.answer = DataManager.shared.fetchAnswers()?.first(where: { $0.type == answerTitle })
                            self.present(nav, animated: true, completion: nil)
                        }
                    }
                }
            })
            section.append(rows: rows)
        }
        
        tableDirector.append(section: section)
        
        tableDirector.reload()
    }
}
