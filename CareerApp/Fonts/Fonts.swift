import UIKit.UIFont

struct FontFamily {
    enum Roboto: String {
        case black = "Roboto-Black"
        case regular = "Roboto-Regular"
        case medium = "Roboto-Medium"
        
        func font(size: CGFloat) -> UIFont {
            return UIFont.init(name: rawValue, size: size)!
        }
    }
}
