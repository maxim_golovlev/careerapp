//
//  QestionView.swift
//  CareerApp
//
//  Created by Admin on 23.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

protocol QestionViewDelegate: class {
    func didSelect(view: QestionView, answer: Question.Option)
}

class QestionStackView: UIStackView {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
       // self.next?.touchesBegan(touches, with: event)
    }
}

class QestionView: UIView {

    @IBOutlet weak private var leftButton: AnswerButton!
    @IBOutlet weak private var rightButton: AnswerButton!
    
    weak var delegate: QestionViewDelegate?
    
    private var leftAnswer: Question.Option?
    private var rightAnswer: Question.Option?
    
    func update(question: Question.Subquestion) {
        self.leftAnswer = question.first
        self.rightAnswer = question.second
        leftButton.textLabel.text = question.first?.text
        rightButton.textLabel.text = question.second?.text
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        _ = [rightButton, leftButton].map({$0?.refresh()})
        
        if  let touch = touches.first,
            let button = touch.view as? AnswerButton,
            let leftAnswer = leftAnswer,
            let rightAnswer = rightAnswer {
            
                switch button.tag {
                    case 1: delegate?.didSelect(view: self, answer: leftAnswer)
                    case 2: delegate?.didSelect(view: self, answer: rightAnswer)
                    default : break
                }
        }
    }
}
