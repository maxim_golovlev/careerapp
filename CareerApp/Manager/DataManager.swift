//
//  DataManager.swift
//  CareerApp
//
//  Created by Admin on 22.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class DataManager {
    
    private init() {}
    static var shared = DataManager()
    var path = Bundle.main.path(forResource: "data", ofType: "plist")
    
    func fetchQuestions() -> [Question]? {
        
        if let path = path {
            if let dict = NSDictionary(contentsOfFile: path) {
            
                if let questionsArray = dict["helpers"] as? [[String: AnyObject]] {
                    let questions = questionsArray.flatMap({ Question.initWith(dict: $0) })
                    return questions
                }
            }
        }
        return nil
    }
    
    func fetchAnswers() -> [Answer]? {
        
        if let path = path {
            if let dict = NSDictionary(contentsOfFile: path) {
                
                if let answerArray = dict["answers"] as? [[String: AnyObject]] {
                    let answers = answerArray.flatMap({ Answer.initWith(dict: $0) })
                    return answers
                }
            }
        }
        return nil
    }
}
