//
//  TestManager.swift
//  CareerApp
//
//  Created by Admin on 24.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class TestManager {
    
    private init() {
        answers = DataManager.shared.fetchAnswers()
    }
    
    static let shared = TestManager()
    
    private var letters = [String]()
    
    let answers: [Answer]?
    
    var maxAnswers = 0
    
    var answer: Answer? {
        
        if letters.count < maxAnswers { return nil }
        
        let bound1 = maxAnswers * 1/4
        var countedSet = NSCountedSet(array: Array(letters[0..<bound1]))
        let first = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        
        let bound2 = maxAnswers * 2/4
        countedSet = NSCountedSet(array: Array(letters[bound1..<bound2]))
        let second = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        
        let bound3 = maxAnswers * 3/4
        countedSet = NSCountedSet(array: Array(letters[bound2..<bound3]))
        let third = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        
        let bound4 = maxAnswers
        countedSet = NSCountedSet(array: Array(letters[bound3..<bound4]))
        let forth = countedSet.max { countedSet.count(for: $0) < countedSet.count(for: $1) } as! String
        
        let code = [first, second, third, forth].joined()
        
        print(code)
        
        return answers?.first{ $0.letters == code }
    }
    
    func append(letter: String, index: Int) {
        
        if letters.indices.contains(index) {
            letters[index] = letter
        } else {
            letters.insert(letter, at: index)
        }

        print(letters)
    }
    
    func refresh() {
        letters.removeAll()
    }
}
