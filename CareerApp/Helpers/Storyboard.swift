//
//  Storyboard.swift
//  CareerApp
//
//  Created by Admin on 21.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardSceneType {
    static var storyboardName: String { get }
    static var controllerName: String { get }
}

extension StoryboardSceneType {
    
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: nil)
    }
    
    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
    
    static func instantiateViewController() -> UIViewController {
        let vc = storyboard().instantiateViewController(withIdentifier: controllerName)
        return vc
    }
}

struct Storyboard {
    
    private init() {}
    
    struct Walkthrough: StoryboardSceneType {
        static var controllerName = ""
        static var storyboardName = "Walkthrough"
    }
    
    struct Qestion: StoryboardSceneType {
        static var controllerName = ""
        static var storyboardName = "Qestions"
    }
    
    struct Help: StoryboardSceneType {
        static var controllerName = "HelpController"
        static var storyboardName = "Help"
    }
    
    struct Answer: StoryboardSceneType {
        static var controllerName = "AnswerController"
        static var storyboardName = "Answers"
    }
    
    struct AnswerDetail: StoryboardSceneType {
        static var controllerName = "AnswerDetailController"
        static var storyboardName = "AnswerDetail"
    }
    
    struct Purchase: StoryboardSceneType {
        static var controllerName = "PurchaseController"
        static var storyboardName = "Purchase"
    }
    
    struct Results: StoryboardSceneType {
        static var controllerName = "ResultsController"
        static var storyboardName = "Results"
    }
}
