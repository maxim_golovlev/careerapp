import Foundation
import UIKit

protocol Alertable {
    func showAlert(title: String?, message: String?)
    func showAlert(title: String?, message: String?, handler: @escaping ((UIAlertAction) -> Void))
    func showAlert(title: String?, message: String?, okayHandler: @escaping ((UIAlertAction) -> Void), cancelHandler: @escaping ((UIAlertAction) -> Void)) 
    func showAlert(title: String?, message: String?, purchaseHandler: @escaping (UIAlertAction) -> Void, /*restoreHandler: @escaping (UIAlertAction) -> Void,*/ cancelHandler: @escaping ((UIAlertAction) -> Void))
}

extension Alertable where Self: UIViewController {
    func showAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String?, message: String?, handler: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let close = UIAlertAction(title: "OK", style: .cancel, handler: handler)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String?, message: String?, okayHandler: @escaping ((UIAlertAction) -> Void), cancelHandler: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okay = UIAlertAction(title: "Повторить", style: .default, handler: okayHandler)
        let close = UIAlertAction(title: "Отмена", style: .cancel, handler: cancelHandler)
        alert.addAction(okay)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
    }
    
    func showAlert(title: String?, message: String?, purchaseHandler: @escaping (UIAlertAction) -> Void,/* restoreHandler: @escaping (UIAlertAction) -> Void,*/ cancelHandler: @escaping ((UIAlertAction) -> Void)) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let purchase = UIAlertAction(title: "Купить", style: .default, handler: purchaseHandler)
       // let restore = UIAlertAction(title: "Восстановить покупки", style: .default, handler: restoreHandler)
        let close = UIAlertAction(title: "Отмена", style: .cancel, handler: cancelHandler)
        alert.addAction(purchase)
     //   alert.addAction(restore)
        alert.addAction(close)
        
        present(alert, animated: true, completion: nil)
        
    }
}
