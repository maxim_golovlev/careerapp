//
//  Shareable.swift
//  CareerApp
//
//  Created by Admin on 07.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

protocol Shareable {
    //func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?)
    func showShareActivity(answer: Answer?)
}

extension Shareable where Self: UIViewController {
    
    func showShareActivity(answer: Answer?) {
        
        if  let profession = answer?.name,
            let goodSides = answer?.sides.good,
            let letters = answer?.letters {
            
            let image = UIImage.init(named: letters.lowercased())
            
            let goodSidesModif = goodSides.components(separatedBy: "\n\n").joined(separator: "\n")
            
            showShareActivity(msg: "Я прошел тест в приложении \"Работа мечты\" и мой тип профессии: \(profession)\nМои сильные стороны:\n \(goodSidesModif)", image: image, url: "https://itunes.apple.com/us/app/работа-мечты-тест/id1332751432?l=ru&ls=1&mt=8", sourceRect: nil)
        }
    }
    
    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        
        var items = [UIActivityItemSource]()
        
        if let msg = msg {
            items.append(MyStringItemSource(textToShare: msg))
        }
        
        if let image = image {
            items.append(MyImageItemSource(image: image))
        }
        
        if let url = url {
            items.append(MyUrlItemSource(urlString: url))
        }
        
        let activityVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
        activityVC.modalPresentationStyle = .popover
        activityVC.popoverPresentationController?.sourceView = view
        activityVC.excludedActivityTypes = [ UIActivity.ActivityType.airDrop,
                                             UIActivity.ActivityType.postToFacebook,
                                             UIActivity.ActivityType.postToFlickr,
                                             UIActivity.ActivityType.postToTwitter,
                                             UIActivity.ActivityType.postToVimeo ]
        
        if let sourceRect = sourceRect {
            activityVC.popoverPresentationController?.sourceRect = sourceRect
        }
        
        present(activityVC, animated: true, completion: nil)
    }
}

class MyStringItemSource: NSObject, UIActivityItemSource {
    
    var textToShare: String!
    
    init(textToShare: String) {
        
        self.textToShare = textToShare
        
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "" as AnyObject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        if activityType == UIActivity.ActivityType.postToTwitter {
            return String() as AnyObject?
            
        } else if activityType?.rawValue == "com.vk.vkclient.shareextension" {
            return textToShare as AnyObject?
            
        } else {
            return textToShare as AnyObject?
        }
    }
}

class MyImageItemSource: NSObject, UIActivityItemSource {
    
    var image: UIImage?
    
    init(image: UIImage?) {
        self.image = image
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return #imageLiteral(resourceName: "placeholder (2)") as UIImage
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        return image
    }
}

class MyUrlItemSource: NSObject, UIActivityItemSource {
    
    var urlString: String?
    
    init(urlString: String?) {
        self.urlString = urlString
    }
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return ""as AnyObject
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        
        return urlString
    }
}
