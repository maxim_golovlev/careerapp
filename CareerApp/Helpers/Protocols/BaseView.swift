import Foundation

protocol BaseView: class, Alertable, Loadable, Shareable { }
