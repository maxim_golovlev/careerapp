//
//  String + Attributed.swift
//  CareerApp
//
//  Created by Admin on 24.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation


extension String {
    
    func getNavbarAttrString() -> NSAttributedString {
        
        let attrString = NSMutableAttributedString(string: self)
        attrString.addAttributes([NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.0007717825938, green: 0.5457701683, blue: 0.5367060304, alpha: 1),
                                  NSAttributedString.Key.font: FontFamily.Roboto.medium.font(size: 17)], range: NSRange.init(location: 0, length: attrString.length))
        
        return attrString
    }
    
}

extension NSMutableAttributedString {
    func set(font: UIFont, text: String? = nil) -> NSMutableAttributedString {
        
        if let text = text {
            self.addAttribute(NSAttributedString.Key.font, value: font, range: (self.string as NSString).range(of: text))

        } else {
            self.addAttribute(NSAttributedString.Key.font, value: font, range: NSMakeRange(0, self.length))
        }
        
        return self
    }
    
    func add(color: UIColor, to slice: String? = nil) -> NSMutableAttributedString {
        
        if let slice = slice {
            let range = (self.string as NSString).range(of: slice)
            self.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        } else {
            self.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: NSMakeRange(0, self.length))
        }
        return self
    }
    
    func addStrikethrough(slice: String) -> NSMutableAttributedString {
        
        let range = (self.string as NSString).range(of: slice)
        self.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2 , range: range)
        return self
    }
    
}
