//
//  Date+Helpers.swift
//  CareerApp
//
//  Created by Admin on 12.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

extension Date {
    func toString(_ format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
