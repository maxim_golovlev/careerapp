//
//  Constants.swift
//  CareerApp
//
//  Created by Admin on 24.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

struct AppConstants {
    
    struct Colors {
        static let redColor = #colorLiteral(red: 0.8603217602, green: 0.3468701243, blue: 0.3252544999, alpha: 1)
        static let lightGreen = #colorLiteral(red: 0.04064219445, green: 0.6354933381, blue: 0.6228145361, alpha: 1)
        static let darkGreen = #colorLiteral(red: 0, green: 0.6192536354, blue: 0.614757359, alpha: 1)
    }
    
    struct NotificationKeys {
        static let transaction = NSNotification.Name(rawValue:"transactionNotification")
    }
}

struct AppScreen {
    static let scale = UIScreen.main.scale
    static let height = UIScreen.main.bounds.size.height
    static let width = UIScreen.main.bounds.size.width
    static let pixel = 1 / UIScreen.main.scale
    static let navbarHeight = 64
    static let tabbarHeight = 49
}


