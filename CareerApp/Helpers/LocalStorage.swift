//
//  LocalStorage.swift
//  CareerApp
//
//  Created by Admin on 08.01.2018.
//  Copyright © 2018 Admin. All rights reserved.
//

import Foundation

class LocalStorage {
    
    private static var stateKey = "state"
    private static var savedAnswersKey = "savedAnswers"
    
    static var ratingIsShown: Bool {
        return UserDefaults.standard.bool(forKey: stateKey)
    }
    
    static func setRatingIsShown(state: Bool) {
        UserDefaults.standard.set(state, forKey: stateKey)
        UserDefaults.standard.synchronize()
    }
    
    static var getAnswers: [SavedAnswer]? {
        guard let data = UserDefaults.standard.object(forKey: savedAnswersKey) as? Data else { return nil }
        return try? JSONDecoder().decode([SavedAnswer].self, from: data)
    }
    
    static func appendAnswer(title: String?, date: Date) {
        
        guard let text = title else { return }

        var answers = getAnswers ?? []
        
        answers.append(SavedAnswer(id: UUID().uuidString, text: text, date: date))
            
        let data = try? JSONEncoder().encode(answers)
        UserDefaults.standard.set(data, forKey: savedAnswersKey)
        UserDefaults.standard.synchronize()
        
    }
}
