//
//  AnswerButton.swift
//  CareerApp
//
//  Created by Admin on 23.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation


class AnswerButton: UIButton {
    
    private var startingPoint: CGPoint!
    private var circle = UIView()
    var textLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = FontFamily.Roboto.medium.font(size: 18)
        label.numberOfLines = 0
        label.textAlignment = .center
      //  label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addSubview(textLabel)
        textLabel.fillSuperview(rightInset: -12, leftInset: 12)
    }
    
    override open var isHighlighted: Bool {
        didSet {
            
        }
    }
    
    func refresh() {
        circle.removeFromSuperview()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.next?.touchesBegan(touches, with: event)
        
        if let touch = touches.first {
            
            if let view = touch.view as? AnswerButton {
                let point = touch.location(in: view)
                circle.frame = frameForCircle(viewSize: view.bounds.size, startPoint: point)
                circle.layer.cornerRadius = circle.frame.height/2
                circle.center = point
                circle.backgroundColor = AppConstants.Colors.redColor
                circle.transform = CGAffineTransform.init(scaleX: 0.001, y: 0.001)
                circle.isUserInteractionEnabled = false
                view.insertSubview(circle, at: 0)
                
                UIView.animate(withDuration: 0.7, animations: {
                    self.circle.transform = .identity
                })
            }
        }
    }
    
    
    func frameForCircle(viewSize: CGSize, startPoint: CGPoint) -> CGRect {
        
        let xLenght = fmax(startPoint.x, viewSize.width - startPoint.x)
        let yLenght = fmax(startPoint.y, viewSize.height - startPoint.y)
        
        let diameter = fmax(xLenght, yLenght)*3
        
        let size = CGSize(width: diameter, height: diameter)
        
        let frame = CGRect(origin: .zero, size: size)
        return frame
    }
}
