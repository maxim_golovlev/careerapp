//
//  QuestionControl.swift
//  CareerApp
//
//  Created by Admin on 23.12.2017.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

class QuestionControl: UIView {
    
    var numberOfItems = 0 {
        didSet {
            for i in 0...(numberOfItems - 1) {
                let view = UIView()
                view.backgroundColor = i == 0 ? selectedColor : defaultColor
                verticalStackView.addArrangedSubview(view)
            }
        }
    }
    
    var selectedColor = UIColor.init(white: 1, alpha: 0.9)
    var defaultColor = UIColor.init(white: 1, alpha: 0.5)
    
    var currentPage = 0 {
        didSet {
            _ = verticalStackView.arrangedSubviews.enumerated().flatMap({ $0.element.backgroundColor = $0.offset == currentPage ? selectedColor : defaultColor })
        }
    }
    
    lazy var verticalStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.alignment = .fill
        sv.distribution = .fillEqually
        sv.spacing = 2
        return sv
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addSubview(verticalStackView)
        verticalStackView.fillSuperview()
        
        currentPage = 0
    }
    
}
